# Tribute to Grace Hopper

Tiny tribute to one of the computer giants, who have allowed me to earn a living doing something as fun and creative as programming.

    Bernard of Chartres used to say that we [the Moderns] are like dwarves perched on the shoulders of giants [the Ancients], and thus we are able to see more and farther than the latter. And this is not at all because of the acuteness of our sight or the stature of our body, but because we are carried aloft and elevated by the magnitude of the giants.

    (John of Salisbury, Metalogicon, Book III, Chapter 4. Cfr. Troyan, Scott D., Medieval Rhetoric: A Casebook, London, Routledge, 2004, p. 10.)



## Motivation

Project [__Build a Tribute page__](https://www.freecodecamp.org/learn/responsive-web-design/responsive-web-design-projects/build-a-tribute-page)
 of the course __[Responsive Web Design Certification](https://www.freecodecamp.org/learn/responsive-web-design/)__ taught by __[freeCodeCamp.org](https://www.freecodecamp.org/)__.





